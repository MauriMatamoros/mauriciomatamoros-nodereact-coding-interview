import people_data from "../data/people_data.json";
import { GetAllPeopleQueryParams } from "../validators/GetAllPeopleQueryParams";

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getByTags = (tags: string[]) => {
    people_data.filter(({ gender, title, company }) => {
      const parsedGender = gender.toLowerCase();
      const parsedTitle = `${title}`.toLowerCase();
      const parsedCompany = company.toLowerCase();
      return tags.find((tag: string) => {
        const parsedTag = tag.toLowerCase();
        return (
          parsedTag === parsedGender ||
          parsedTag === parsedTitle ||
          parsedTag === parsedCompany
        );
      });
    });
  };

  getAll(query: GetAllPeopleQueryParams) {
    console.log(query);
    const { page, pageSize = 10 } = query;
    const totalPages = Math.ceil(people_data.length / pageSize);
    const startIndex = (page - 1) * pageSize;
    const endIndex = startIndex + pageSize;
    return { people: people_data.slice(startIndex, endIndex), totalPages };
  }
}
