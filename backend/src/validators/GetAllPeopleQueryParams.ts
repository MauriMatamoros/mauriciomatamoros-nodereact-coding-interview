import { IsNumber, IsOptional } from "class-validator";

export class GetAllPeopleQueryParams {
  @IsNumber()
  page: number;

  @IsOptional()
  @IsNumber()
  pageSize: number;
}
