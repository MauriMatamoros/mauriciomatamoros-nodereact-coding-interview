import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress, Autocomplete, TextField, Chip } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [tags, setTags] = useState<string[]>();
  const [page, setPage] = useState<number>(1);
  const [loading, setLoading] = useState<boolean>(true);
  const pageSize = 10;

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers();
      setUsers(result.data);
    };

    fetchData()
      .then(() => setLoading(false))
      .catch(() => setLoading((prevState) => !prevState));
  }, []);

  const handleAutoCompleteOnChange = (
    e: React.ChangeEvent<unknown>,
    value: string[]
  ) => {
    setTags(value);
  };

  const handlePaginationOnChange = (
    e: React.ChangeEvent<unknown>,
    value: string[]
  ) => {};

  return (
    <div style={{ paddingTop: "30px" }}>
      <Autocomplete
        multiple
        options={[]}
        freeSolo
        onChange={handleAutoCompleteOnChange}
        renderTags={(value: readonly string[], getTagProps) =>
          value.map((option: string, index: number) => (
            <Chip
              variant="outlined"
              label={option}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            {...params}
            variant="filled"
            label="freeSolo"
            placeholder="Favorites"
          />
        )}
      />
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
        <div>
          <Typography>Page: {page}</Typography>
          <Pagination count={} page={page} onChange={handleChange} />
        </div>
      </div>
    </div>
  );
};
